#########################################################
# File: framework.h
#
#This file is part of Model-loader
#
#Copyright (C) 1999 David Stibbe (dstibbe@gmail.com)
#                     and Gerrit Jan Jansen
#
#Stellar is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#########################################################

CC		= gcc
COMPILEFLAGS	=
LINKFLAGS	=
CCFLAGS		= -O2
CPPFLAGS	= -O2
MKDEPEND	= -MM
INCDIR		= -I/usr/include
LIBDIR		= -L/usr/X/lib -L/usr/lib
LIB             = -lm -lfltk -lMesaGLU -lMesaGL -lX11 -lXext -lXmu -lXi -lnsl -lsocket

CPPSOURCES = framework.cpp main.cpp

opdracht5:	$(CPPSOURCES:.cpp=.o) makefile
		$(CC) $(CPPFLAGS) $(LIBDIR) $(LINKFLAGS) $(LIB) -o fltk-model-viewer $(CPPSOURCES:.cpp=.o) 

%.o:    %.cpp
	$(CC) $(INCDIR) $(CPPFLAGS) $(COMPILEFLAGS) -c $(@:.o=.cpp)
