/****************************************
File: framework.h

This file is part of Model-loader

Copyright (C) 1999 David Stibbe (dstibbe@gmail.com)
                     and Gerrit Jan Jansen

Stellar is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
****************************************/
#ifdef WIN32
#define M_PI 3.14159265358979323846
#define M_E 2.71828182845904523536
#endif


#ifndef _OPDR5_FRAMEWORK_H_
#define _OPDR5_FRAMEWORK_H_

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <FL/Fl.H>
#include <FL/gl.h>
#include <FL/Fl_Gl_Window.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Value_Slider.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Output.H>
#include <FL/Fl_Menu_Button.H>
#include <FL/Fl_Menu_Item.H>
#include <GL/glu.h>

#define WIREFRAME 0
#define USE_COLOR 1
#define USE_NORMAL 2
#define USE_NORMAL_SMOOTH 3


#define M_X 0
#define M_Y 1
#define M_Z 2

typedef struct vertex_s {
  float pos[3];	// x, y en z positie van vertex
  float normal[3];      // normaal van dit vertex , moet je zelf uitrekenen
} vertex;

typedef struct face_s {
  int vertex[3];	// index van de drie vertices van face
  float normal[3]; // normaal van dit face, moet je zelf uitrekenen
} face;

extern int numberOfVertices;	// aantal vertices in het model
extern vertex *vertices;			// alle vertices van het model
extern int numberOfFaces;		// aantal faces in het model
extern face *faces;				// alle faces van het model


// readModel() leest een .cgm file en slaat die op in de bovenstaande arrays 'vertices' en 'faces'
// Bij succes geeft readModel() true terug, anders false.
int readModel(const char *filename);

// de waarde van de rotInc slider onderaan het window
extern float rotationIncrement;
// Als je op de playbutton drukt, wordt 'running' true
extern int running;

// Deze 6 functies mogen ingevuld worden...
extern void mouseButtonDown(float x, float y);  // wordt aangeroepen als je een knopt indrukt
extern void mouseButtonUp(float x, float y);    // wordt aangeroepen als je een knopt loslaat
extern void mouseDrag(float x, float y);        // wordt aangeroepen als je de muis beweegt met een knop ingedrukt

extern void doDrawing(int opdracht);                        // moet AL het tekenwerk doen
extern void step(float rotationIncrement);                             // wordt aangeroepen als de positie van het figuur veranderd moet worden
extern void reset();                            // wordt aangeroepen als de positie van het figuur terug naar af moet

extern void loadModel(int numberOfVertices,vertex* vertices,int numberOfFaces,face* faces );

extern void drawWireFrame();
extern void drawColorFrame();
extern void drawFillFrame();
extern void drawFillSmoothFrame();

extern void transform();

// deze functie maakt een window met daarin een OpenGL window en een control bar
extern void createWindow(int argc, char *argv[]);

// deze functie zorgt dat het OpenGL window zichzelf opnieuw tekent
extern void redraw();


void plotAxes();
int viewFront();
int viewBack();

extern void zoom(float z);
extern void moveX(float da);
extern void moveY(float da);
extern void normalVector(float e1[],float e2[],float e3[]);

/*
GLWindow is een C++ class afgeleid van Fl_Gl_Window.
In de fltk-toolkit worden alle windows waarin je OpenGL
wilt gebruiken afgeleid van Fl_Gl_Window.

De fltk-toolkit regelt alle 'details' zoals het maken
van een OpenGL context. Het enige wat je zelf hoeft te
doen voor een minimale applicatie is een 'draw()' en
mogelijk een 'handle()' functie implementeren.
*/
class GLWindow : public Fl_Gl_Window {
public:
	// constructor (roept alleen constructor van Fl_Gl_Window aan)
	GLWindow(int X, int Y, int W, int H, const char *L) : Fl_Gl_Window(X, Y, W, H, L) {};

	void draw();
	int handle(int);

private:
	/*
	beginDraw() en endDraw() zijn twee hulpfuncties die oninteressante dingen doen.
	*/
	void beginglDraw();
	void endglDraw();
};

#endif // _OPDR5_FRAMEWORK_H_
